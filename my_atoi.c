#include <stdio.h>

int my_atoi(char *);
int my_pow(int, int);
int my_strlen(char *);

int main(int argc, char **argv) {
  if (argc < 2) {
    printf("Usage: %s <numeric_string>\n\n", argv[0]);
    return 0;
  }

  printf("\"%s\" = %d\n", argv[1], my_atoi(argv[1]));

  return 0;
}

int my_strlen(char *s) {
  if (s == NULL) {
    return -1;
  }
  char *tmp = s;
  while (*s != '\0') {
    s++;
  }
  return s - tmp;
}

int my_pow(int n, int exp) {
  int tmp = n;
  while (exp > 1) {
    n = tmp * n;
    exp--;
  }
  return n;
}

int my_atoi(char *s) {

  // check if s is NULL
  if (s == NULL) {
    printf("NULL passed, exiting...\n");
    return 0;
  }
     
  int len = my_strlen(s),
      i = 0,
      n = 0,
      places = len;

  // negative number
  if (s[0] == '-') {
    i = 1;
    places = len - 1;
  }

  while (i < len) {
    // not a numeric char
    if (s[i] < 48 && s[i] > 57) {
      break;
    }
    if (places > 0) {
    n += (s[i] - 48) * my_pow(10, places);
    } else {
      n += s[i] - 48;
    }
    places--;
    i++;
  }

  // hacky, but allows proper int output
  n /= 10;

  return s[0] == '-' ? n - (n * 2) : n;
}
